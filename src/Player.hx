import starling.events.*;
import starling.display.*;
import starling.animation.DelayedCall;
import flash.ui.Keyboard;
import Math;
import Game;
import World;
import Geom;
import Health;
import Score;
import Root;
using Geom.Vec2Ext;
using Geom.RectExt;

class Player extends Entity {

    ////////////////////////////////////////////
    // CONSTANTS
    public static var BODY_FRICTION = 0.2;
    public static var STAND_FRICTION = 1;
    public static var WALK_FRICTION = 0.9;

    public static var WALK_IMP : Vec2 = {x:18, y:0}
    public static var DRIFT_IMP : Vec2 = {x:5, y:0}
    public static var JUMP_IMP : Vec2 = {x:0, y:-8}

    public var WHIP_WINDUP : Float = 0.44;
    public var WHIP_WINDDOWN : Float = 0.38;
    public var WHIP_DURATION : Float = 0.82;

    public var HURT_DURATION : Float = 0.6;
    public var HURT_INV_DURATION : Float = 2.0;

    ////////////////////////////////////////////
    // Properties
    public var keycodes = {
        up: new Array<Int>(),
        down: new Array<Int>(),
        left: new Array<Int>(),
        right: new Array<Int>(),
        jump: new Array<Int>(),
        whip: new Array<Int>(),
    }

    public var keydown = {
        up : false,
        down : false,
        left : false,
        right : false,
        jump : false,
        whip : false,
    }

    public var paused : Bool = false;
    public var jumping : Bool = false;
    public var stand_ts : Float = -1;
    public var jump_ts : Float = -1;
    public var whip_ts : Float = -1;
    public var hurt_ts : Float = -1;
    public var death_ts : Float = -1;
    //public var player_state : PlayerState = Stand;
    public var facing : Float = 1;
    private var walk_norm : Vec2 = {x:0,y:0};

    var stand_whip_animation : MovieClip;
    var health : Health;
    var score : Score;
    var world : World;


    ////////////////////////////////////////////
    // Public functions
    
    public function new(){
        super();
        debugColor = 0x0000FF; //0xFFFFFF;
        setHitbox({x:-0.3, y:-0.8, w:0.6, h:1.8});
        density = 1;
        restitution = 0.1;
        friction = BODY_FRICTION;
        title = "player";

        var animation = Root.buildWorldMovieClip("player_idle.");
        animation.pivotX = 16;
        animation.pivotY = 32;
        animation.setFrameDuration(0,.6);
        animation.setFrameDuration(1,.1);
        animation.setFrameDuration(2,.6);
        animation.setFrameDuration(3,.1);
        addChild(animation);
        addAnimation("idle", animation);

        animation = Root.buildWorldMovieClip("player_run.");
        animation.pivotX = 16;
        animation.pivotY = 32;
        animation.setFrameDuration(0,.11);
        animation.setFrameDuration(1,.15);
        animation.setFrameDuration(2,.11);
        animation.setFrameDuration(3,.15);
        addChild(animation);
        addAnimation("run", animation);

        animation = Root.buildWorldMovieClip("player_jump.");
        animation.pivotX = 16;
        animation.pivotY = 32;
        addChild(animation);
        addAnimation("jump", animation);

        animation = Root.buildWorldMovieClip("player_fall.");
        animation.pivotX = 16;
        animation.pivotY = 32;
        addChild(animation);
        addAnimation("fall", animation);

        animation = Root.buildWorldMovieClip("player_hurt.");
        animation.pivotX = 16;
        animation.pivotY = 32;
        addChild(animation);
        addAnimation("hurt", animation);

        animation = Root.buildWorldMovieClip("player_death.");
        animation.pivotX = 42;
        animation.pivotY = 32;
        animation.setFrameDuration(0,.3);
        animation.setFrameDuration(1,.2);
        animation.setFrameDuration(2,1.0);
        animation.loop = false;
        addChild(animation);
        addAnimation("death", animation);

        animation = Root.buildWorldMovieClip("player_whip.");
        animation.pivotX = 81;
        animation.pivotY = 60;
        animation.setFrameDuration(0,.12);
        animation.setFrameDuration(1,.12);
        animation.setFrameDuration(2,.10);
        animation.setFrameDuration(3,.10);
        animation.setFrameDuration(4,.14);
        animation.setFrameDuration(5,.12);
        animation.setFrameDuration(6,.12);
        addChild(animation);
        addAnimation("whip", animation);
        stand_whip_animation = animation;

        animation = Root.buildWorldMovieClip("player_air_whip.");
        animation.pivotX = 81;
        animation.pivotY = 60;
        animation.setFrameDuration(0,.12);
        animation.setFrameDuration(1,.12);
        animation.setFrameDuration(2,.10);
        animation.setFrameDuration(3,.10);
        animation.setFrameDuration(4,.14);
        animation.setFrameDuration(5,.12);
        animation.setFrameDuration(6,.12);
        addChild(animation);
        addAnimation("air-whip", animation);

        var animation = Root.buildWorldMovieClip("player_male_idle.");
        animation.pivotX = 16;
        animation.pivotY = 32;
        animation.x = 8/24;
        animation.y = 16/24;
        animation.setFrameDuration(0,.5);
        animation.setFrameDuration(1,.5);
        animation.setFrameDuration(2,.2);
        animation.setFrameDuration(3,.3);
        addChild(animation);
        addAnimation("idle_male", animation);

        playAnimation("idle");

        keycodes.up.push(Keyboard.W);
        keycodes.up.push(Keyboard.UP);
        keycodes.down.push(Keyboard.S);
        keycodes.down.push(Keyboard.DOWN);
        keycodes.left.push(Keyboard.A);
        keycodes.left.push(Keyboard.LEFT);
        keycodes.right.push(Keyboard.D);
        keycodes.right.push(Keyboard.RIGHT);
        keycodes.jump.push(Keyboard.SPACE);
        keycodes.whip.push(Keyboard.SLASH);

        Root.singleton.addEventListener(KeyboardEvent.KEY_DOWN, keyDown);
        Root.singleton.addEventListener(KeyboardEvent.KEY_UP, keyUp);
        addEventListener(Event.ADDED, function(){
                removeEventListeners(Event.ADDED);
                world = cast(parent,World);
                var game = world.game;
                health = game.overlay.health;
                score = game.overlay.score;
                world.addPreCollisionListener(this, preCollision);
                world.addPostCollisionListener(this, postCollision);
                });
    }

    function keyDown(event : KeyboardEvent){
        if(keycodes.up.indexOf(event.keyCode) >= 0) keydown.up = true; 
        if(keycodes.down.indexOf(event.keyCode) >= 0) keydown.down = true; 
        if(keycodes.left.indexOf(event.keyCode) >= 0) keydown.left = true; 
        if(keycodes.right.indexOf(event.keyCode) >= 0) keydown.right = true; 
        if(keycodes.jump.indexOf(event.keyCode) >= 0) keydown.jump = true; 
        if(keycodes.whip.indexOf(event.keyCode) >= 0) keydown.whip = true; 
    }

    function keyUp(event : KeyboardEvent){
        if(keycodes.up.indexOf(event.keyCode) >= 0) keydown.up = false; 
        if(keycodes.down.indexOf(event.keyCode) >= 0) keydown.down = false; 
        if(keycodes.left.indexOf(event.keyCode) >= 0) keydown.left = false; 
        if(keycodes.right.indexOf(event.keyCode) >= 0) keydown.right = false; 
        if(keycodes.jump.indexOf(event.keyCode) >= 0) keydown.jump = false; 
        if(keycodes.whip.indexOf(event.keyCode) >= 0) keydown.whip = false; 
    }

    function preCollision(info : CollisionInfo, e1 : Entity, e2 : Entity){
        var other = (e1 == this)? e2 : e1;
        var order_coef = (e1 == this)? 1 : -1;
        if(other.title == "blast") takeDamage(other);
        if(other.title == "coin") collectCoin(other);
        if(other.title == "healthpack") getHealth(other);
        if(other.title == "goal" && !paused) {
            world.game.endLevel(other);
        }
        if(info.enabled == false) return;
        if(info.normal.y*order_coef > 0 && 
                info.normal.y*order_coef > Math.abs(info.normal.x)) {
            info.restitution = 0;
            info.friction = STAND_FRICTION;
            if(walk_norm.mag() != 0) info.friction = WALK_FRICTION;
            //stand_ts = elapsedTime;
        }

        if(other.title == "enemy") takeDamage(other);
        if(other.title == "damage") takeDamage(other);
        return;
    }

    function postCollision(info : CollisionInfo, e1 : Entity, e2 : Entity){
        var other = (e1 == this)? e2 : e1;
        var order_coef = (e1 == this)? 1 : -1;
        if(velocity.y > -0.1 &&
                info.normal.y*order_coef > 0 && 
                info.normal.y*order_coef > Math.abs(info.normal.x)) {
            stand_ts = elapsedTime;
        }
        if(Math.abs(info.result_imp) > 18) takeDamage((e1 == this)? e1 : e2);
        //trace("post");
        //info.enabled = false;
    }

    function takeDamage(source : Entity) {
        // Make sure we aren't invincible
        if(elapsedTime - hurt_ts < HURT_INV_DURATION) return;
        if(paused) return;
        // Decrement health
        health.loseHealth();
        // Knockback from damage source
        var knockback_dir : Float = (source.hitbox.center().x > hitbox.center().x) ? -1 : 1;
        if(source.hitbox.center().x == hitbox.center().x) knockback_dir = 0;
        impulse = impulse.add({x:knockback_dir,y:-1.0}.mult(3));

        if(!health.isDead()){
            // If we aren't dead play our hurt animation and invincibility
            hurt_ts = elapsedTime;
            playAnimation("hurt",{x:facing,y:1});
            Root.assets.playSound("Hurt1");
            var delay = 0.08;
            var flashcall = new DelayedCall(function(){
                    alpha = 1-alpha;
                    },delay);
            flashcall.repeatCount = Std.int(HURT_INV_DURATION/delay);
            flashcall.repeatCount += flashcall.repeatCount % 2;
            juggler.add(flashcall);
        } else {
            // If we are dead, call it and setup a call to gameover
            death_ts = elapsedTime;
            paused = true;
            playAnimation("death",{x:facing,y:1});
            Root.assets.playSound("Death1");
            removeEventListeners();
            juggler.delayCall(function() {
                    //trace("Game Over");
                    Root.singleton.showGameOver(false);
                    
                    }, 5.0);
            removeEventListeners();
        }
    }

    function collectCoin(source : Entity) {
        // Make sure we aren't invincible
        if(paused) return;
        // Increase Score
        score.addPoints(100);
    }

    function getHealth(source : Entity) {
        // Make sure we aren't invincible
        if(paused) return;
        // Increase Health
        //trace("addHealth");
        health.addHealth();
    }
    override public function advanceTime(t:Float){
        super.advanceTime(t);

        if(getHitbox().center().y > world.map_height) takeDamage(this);

        var whipping = false;

        var whipping = (elapsedTime - whip_ts < WHIP_DURATION) ? true : false;
        var hurt = (elapsedTime - hurt_ts < HURT_DURATION) ? true : false;
        var dead = (death_ts > 0) ? true : false;
        var locked = whipping || hurt || dead || paused;

        var state : PlayerState = Stand;
        if(elapsedTime - jump_ts > 0.1 && elapsedTime - stand_ts < 0.1) state = Stand;
        else state = Jump;

        // Determine directon
        var direction = 0;
        if(!locked){
            if(keydown.right && !keydown.left) direction = 1;
            else if(keydown.left && !keydown.right) direction = -1;
        }
        if(direction != 0) facing = direction;

        if(state == Stand) {
            if(locked){
                if(current_animation=="air-whip") playAnimation("whip",{x:facing,y:1});
            }
            // Check for jumping
            else if(!jumping && keydown.jump && impulse.mag() < 0.1) {
                impulse = impulse.add(JUMP_IMP.mult(getMass()));
                jump_ts = elapsedTime;
                jumping = true;
                playAnimation("jump",{x:facing,y:1});
                state = Jump;
                Root.assets.playSound("Jump1");
            }
            else if(keydown.whip) {
                trigger_whip();
                whip_ts = elapsedTime;
                playAnimation("whip",{x:facing,y:1});
            }
            else {
                // Left right motion
                var walk_imp = WALK_IMP.mult(direction);
                if(direction == 1) walk_imp.x = Math.min(walk_imp.x, walk_imp.x-velocity.x);
                else walk_imp.x = Math.max(walk_imp.x, walk_imp.x-velocity.x);
                walk_imp = walk_imp.mult(t);
                impulse = impulse.add(walk_imp);
                walk_norm = {x:direction, y:0};
                if(direction == 0) playAnimation("idle", {x:facing, y:1});
                else playAnimation("run", {x:facing,y:1});
            }
        }
        if(state == Jump) {

            impulse = impulse.add(DRIFT_IMP.mult(direction*t));
            if(locked) {
            }
            else if(keydown.whip) {
                trigger_whip();
                whip_ts = elapsedTime;
                playAnimation("air-whip",{x:facing,y:1});
                stand_whip_animation.stop();
                stand_whip_animation.play();
            }
            else if(!jumping && velocity.y > 0) playAnimation("fall", {x:facing, y:1});
            // If we released our jump, push down a bit, depending on how early it was
            else if(!keydown.jump && jumping) {
                var jump_mag = JUMP_IMP.mag();
                var jump_power = Math.max(0,(1 + jump_ts - elapsedTime));
                if(velocity.project(JUMP_IMP).mag()/jump_mag > 0.5){
                    impulse = impulse.add(JUMP_IMP.mult(-jump_power*0.40));
                }
                jumping = false;
            }
        }

        if(!keydown.jump) jumping = false;
    }

    private function trigger_whip(){
        var t = new Entity();
        t.debugColor = 0xFF0000;
        t.title = "whip_damage";
        t.alpha = 0.2;
        t.solid = false;
        t.dynam = true;
        t.setHitbox({x:(0.5+2)*facing-2,y:-1.25,w:4,h:1.5});
        juggler.delayCall(function() { 
                t.x = x;
                t.y = y;
                parent.addChild(t);
                },WHIP_WINDUP);
        juggler.delayCall(function() { 
                parent.removeChild(t,true);
                },WHIP_WINDUP+0.1);
        Root.assets.playSound("Whip2");
    }
}

enum PlayerState {
    Stand;
    Jump;
}

