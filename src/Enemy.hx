import starling.events.*;
import flash.ui.Keyboard;
import starling.textures.Texture;
import starling.textures.TextureAtlas;
import Math;
import Game;
import World;
import Geom;
import Blast;
import Root;
using Geom.Vec2Ext;
using Geom.RectExt;

class Enemy extends Entity {

	public static var BODY_FRICTION = 0.2;
    public var health = 3;
    public static var LEFT = 2;
    public static var RIGHT = 2;
    var blast_on:Int = 0;
    var blast_dir:Int = 1;
    var blast_time:Int = 0;
    var DIRECTION:Int = 1;
    var time:Int = 0;
    var unseen:Int = 1;
    var x_check:Float = 0;
    var ori_x:Float = 0;
    public var blast: Blast;
    public var WHIP_WINDUP : Float = 0.44;

    var world:World;

	public function new(){
        super();
        debugColor = 0x666666;
        setHitbox({x:-0.43, y:-1, w:.7, h:2});
        density = 3;
        restitution = 0;
        friction = BODY_FRICTION;
        title = "enemy";


        var animation = Root.buildWorldMovieClip("enemy1_walk");
        animation.pivotX = 16;
        animation.pivotY = 32;
        animation.setFrameDuration(1,.75);
        animation.setFrameDuration(2,.75);
        animation.setFrameDuration(3,.75);
        animation.setFrameDuration(4,.75);
        addChild(animation);
        addAnimation("walk", animation);

        playAnimation("walk");

        animation = Root.buildWorldMovieClip("enemy1_walk2");
        animation.pivotX = 16;
        animation.pivotY = 32;
        animation.setFrameDuration(1,.75);
        animation.setFrameDuration(2,.75);
        animation.setFrameDuration(3,.75);
        animation.setFrameDuration(4,.75);
        addChild(animation);
        addAnimation("walk2", animation);

        animation = Root.buildWorldMovieClip("blast1");
        animation.pivotX = 16;
        animation.pivotY = 32;
        animation.setFrameDuration(0,.75);
        addChild(animation);
        addAnimation("blast1", animation);

        addEventListener(Event.ADDED, function(){
                removeEventListeners(Event.ADDED);
                world = cast(parent, World);
                ori_x = x;
                world.addPreCollisionListener(this, preCollision);
                });

        this.addEventListener(EnterFrameEvent.ENTER_FRAME, walker);
        
	}

    function preCollision(info : CollisionInfo, e1 : Entity, e2 : Entity){
        var other = e2;
        if(this == e2) other = e1;
        if(other.title == "whip_damage") {
            Root.assets.playSound("Enemy_Hurt");
            health--;
            if (health==0){
                if(blast_on == 1){
                    world.removeChild(blast, true);  
                }
                removeFromParent(true);
            }
        }
    }

    public function walker() {
        if(blast_time > 60){
            if(blast.x > blast.ori_x+4 && blast.DIRECTION == 1){
                world.removeChild(blast, true);  
                blast_on = 0; 
                blast_time = 0; 
            }
            if(blast.x < blast.ori_x-4 && blast.DIRECTION == 0){
                world.removeChild(blast, true);
                blast_on = 0;
                blast_time = 0; 
            }
        }
        if(blast_on == 1){
            if(blast.DIRECTION == 1){
                blast.x = blast.x + .05;
                blast_time = blast_time + 1;    
            }
            else {
                blast_time = blast_time + 1;
                blast.x = blast.x - .05;    
            }
        }
        if(unseen == 0){
            if(world.player.x > x && DIRECTION == 1 && world.player.x < x + 4) {
                if (blast_on == 0) {
                    trigger_blast();
                }
            }
            else if(world.player.x > x && DIRECTION == 1){
                x = x + .02;                   
            }
            else if(world.player.x > x && DIRECTION == 0){
                x = x + .02;
                DIRECTION = 1;
                playAnimation("walk");
            }
            else if(world.player.x < x && DIRECTION == 0 && world.player.x > x - 4) {
                if (blast_on == 0) {
                    trigger_blast();
                }
            }
            else if(world.player.x < x && DIRECTION == 0){
                x = x - .02;
            }
            else {
                DIRECTION = 0;
                playAnimation("walk2");
                x = x - .02;
            }        
        }
        else {
            if(world.player.x < x+4 && world.player.x > x && DIRECTION == 1){
                unseen = 0;
            }
            else if(world.player.x > x-4 && world.player.x < x && DIRECTION == 0) {
                unseen = 0;
            }
            time = time + 1;
            if(time == 1){
                if(x > ori_x + RIGHT && DIRECTION == 1){
                    x = x - .02;
                    DIRECTION = 0;
                    playAnimation("walk2");
                }
                else if(DIRECTION == 1 && x <= ori_x+RIGHT){
                    x = x + .02;
                }
                else if(DIRECTION == 0 && x >= ori_x-LEFT){
                    x = x - .02;
                }
                else if(x < ori_x-LEFT && DIRECTION == 0) {
                    x = x + .02;
                    DIRECTION = 1;
                    playAnimation("walk");
                }
                time = 0;    
            }
        }
    }
    private function trigger_blast(){
        blast = new Blast();
        blast.x = x;
        blast.y = y-.5;
        blast.ori_x = x;
        if(world.player.x > x){
            blast.DIRECTION = 1;
        }
        else{
            blast.DIRECTION = 0;
        }
        world.addChild(blast);
        world.juggler.add(blast);
        blast_on = 1;
        blast_time = 0;
    }
}
