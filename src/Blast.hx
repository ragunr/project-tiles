import starling.events.*;
import flash.ui.Keyboard;
import starling.display.*;
import starling.textures.Texture;
import starling.textures.TextureAtlas;
import Math;
import Game;
import World;
import Geom;
import Root;
using Geom.Vec2Ext;
using Geom.RectExt;

class Blast extends Entity {

	public var DIRECTION:Int = 0;
	public var ori_x:Float;

	public function new(){
        super();
        solid = false;
        debugColor = 0x0;
        setHitbox({x:.2, y:.2, w:.6, h:.6});
        density = 0;
        restitution = 0;
        title = "blast";

        // var animation = Root.buildWorldMovieClip("blast1");
        // addChild(animation);
        // addAnimation("blast", animation);

        var animation = Root.buildWorldMovieClip("blast1");
        animation.setFrameDuration(0,.05);
        animation.setFrameDuration(1,.05);
        addChild(animation);
        addAnimation("blast", animation);

        playAnimation("blast");
        Root.assets.playSound("Blast");
    }

}