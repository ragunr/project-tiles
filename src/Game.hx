import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.textures.TextureSmoothing;
import flash.geom.*;
import flash.media.SoundChannel;
import World;
import Geom;
import Overlay;
import Dialogue;
using Geom.Vec2Ext;
using Geom.RectExt;
using Lambda;



// Top level game class. This holds the physics enabled world and keeps
// track of all of the major game pieces.
class Game extends AnimSprite {

    // composite - container to hold the world an paralax layers around it
    private var composite : Sprite;
    // Physics enabled world
    public var world : World;
    public var overlay: Overlay;
    public var music_channel : SoundChannel;

    public function new() {
        super();

        composite = new Sprite();

        var background = new Image(Root.assets.getTexture(Root.singleton.levelBackgrounds[Root.singleton.currentLevel]));
        var background = new Image(Root.assets.getTexture(Root.singleton.levelBackgrounds[Root.singleton.currentLevel]));
        background.smoothing = TextureSmoothing.NONE;
        background.scaleX = background.scaleY = 2;
        composite.addChild(background);

        overlay = new Overlay();

        world = new World(this,new Point(0,9.8));
        world.scaleX = 24*2;
        world.scaleY = 24*2;

        composite.addChild(world);
        addChild(composite);

        //World.build_test_scene(world);
        var tilemap = new Tilemap(world, Root.assets, Root.singleton.levels[Root.singleton.currentLevel]);
        Root.singleton.playMusic("music_cave");

        juggler.add(world);
        addChild(overlay);

        play_scene("level_"+Root.singleton.currentLevel+"_intro");
    }

    public function endLevel(target:Entity=null) {
        var callback = function(){
            Root.singleton.showContinue();
        }
        Root.singleton.playMusic("music_theme");
        

        play_scene("level_"+Root.singleton.currentLevel+"_end",callback,target);
    }

    function buildPlayerPortrait():MovieClip {
        var talk_movie = new MovieClip(
                Root.assets.getTextures("player_portrait_talk."),
                4);
        talk_movie.smoothing = TextureSmoothing.NONE;
        talk_movie.scaleX = 4;
        talk_movie.scaleY = 4;
        talk_movie.y = 10;
        juggler.add(talk_movie);
        return talk_movie;
    }

    function buildHcPortrait():MovieClip {
        var talk_movie = new MovieClip(
                Root.assets.getTextures("hc_talk."),
                4);
        talk_movie.smoothing = TextureSmoothing.NONE;
        talk_movie.scaleX = 4;
        talk_movie.scaleY = 4;
        talk_movie.y = 10;
        juggler.add(talk_movie);
        return talk_movie;
    }

    function play_scene(scene_name:String, callback:Void->Void=null, target:Entity=null) {
        if(callback == null) callback = function(){};
        world.player.paused = true;
        if(scene_name == "level_1_intro"){
            var d1 = new Dialogue(
                    "Ariza Han",
                    ["\"The Temporal Sckism worked, I have returned to the distant past, in the days of prehistory.\"",
                    "\"The Earth hasn't yet been destroyed, and I can save artifacts from their coming anihalation!\"",
                    "\"But first I will need to fight agaisnt the post-ozone mutants that inhabit end earth!\""].list(),
                    buildPlayerPortrait());

            juggler.delayCall(function(){
                    juggler.add(d1);
                    },1.0);
            addChild(d1);
            
            d1.onComplete = function(){
                removeChild(d1);
                juggler.remove(d1);
                world.player.paused = false;
                callback();
            }
        } else if(scene_name == "level_1_end") {
            var d1 = new Dialogue(
                    "Ariza Han",
                    ["\"Can it be?! The Golden Spear of the Bush Clan! This belongs in a space museum!\"",
                    "\"Passed from father to son, this is the spear used to slay the Hussainian leader by the Bush himself!\""
                    ].list(),
                    buildPlayerPortrait());

            juggler.delayCall(function(){
                    juggler.add(d1);
                    },1.0);
            addChild(d1);
            
            d1.onComplete = function(){
                removeChild(d1);
                juggler.remove(d1);
                callback();
            }
        } else if(scene_name == "level_2_intro") {
            var d1 = new Dialogue(
                    "Ariza Han",
                    ["\"My Time Scanner has determined a artifact adrift on this astroid. I must locate it.\""
                    ].list(),
                    buildPlayerPortrait());

            juggler.delayCall(function(){
                    juggler.add(d1);
                    },1.0);
            addChild(d1);
            
            d1.onComplete = function(){
                removeChild(d1);
                juggler.remove(d1);
                world.player.paused = false;
                callback();
            }
        } else if(scene_name == "level_2_end") {
            var d1 = new Dialogue(
                    "Ariza Han",
                    ["\"I can not believe it! It is the Golden Cybernetic arm of Barak Obama, the cyberking!\"",
                    "\"He lost this arm durring the battle where he defeated the Osama at Benghazi and...\"",
                    "\"accidently released the deadly insectoid Isi!\""
                    ].list(),
                    buildPlayerPortrait());

            juggler.delayCall(function(){
                    juggler.add(d1);
                    },1.0);
            addChild(d1);
            
            d1.onComplete = function(){
                removeChild(d1);
                juggler.remove(d1);
                callback();
            }
        } else if(scene_name == "level_3_intro") {
            var d1 = new Dialogue(
                    "Ariza Han",
                    ["\"My equipment has located a disturbance here! I must unlock this mystery.\""
                    ].list(),
                    buildPlayerPortrait());

            juggler.delayCall(function(){
                    juggler.add(d1);
                    },1.0);
            addChild(d1);
            
            d1.onComplete = function(){
                removeChild(d1);
                juggler.remove(d1);
                world.player.paused = false;
            }
        } else if(scene_name == "level_3_end") {
            var d1 = new Dialogue(
                    "Ariza Han",
                    ["\"Impossible! It is Battle Maiden Clinton, frozen in carbonite by MECHA-PUTIN at the...\"",
                    "\"end of the Maddness wars... she is still alive! I think I can disable the chamber!\""
                    ].list(),
                    buildPlayerPortrait());

            var d2 = new Dialogue(
                    "Hillary Clinton",
                    ["\"*cough* ... A time hunter?! When are you from!\"",
                    ].list(),
                    buildHcPortrait());

            var d3 = new Dialogue(
                    "Ariza Han",
                    ["\"... The 7th century of the Phason Age...\"",
                    ].list(),
                    buildPlayerPortrait());

            var d4 = new Dialogue(
                    "Hillary Clinton",
                    ["\"Good! Then there is still time! You must take me back with you...\"",
                    "\"While there is still time to stop MECHA-PUTIN, and save our worlds!\""
                    ].list(),
                    buildHcPortrait());


            juggler.delayCall(function(){
                    juggler.add(d1);
                    },1.0);
            addChild(d1);

            var thaw_complete = function(){
                addChild(d2);
                juggler.add(d2);
            }
            
            d1.onComplete = function(){
                removeChild(d1,true);
                juggler.remove(d1);
                target.layerMid.removeChildren();
                var thaw_animation = Root.buildWorldMovieClip("hc_unfreeze");
                thaw_animation.alignPivot();
                thaw_animation.setFrameDuration(0,0.5);
                thaw_animation.setFrameDuration(1,0.1);
                thaw_animation.setFrameDuration(2,0.2);
                thaw_animation.setFrameDuration(3,0.2);
                thaw_animation.setFrameDuration(4,0.5);
                thaw_animation.setFrameDuration(5,0.5);
                target.layerMid.addChild(thaw_animation);
                juggler.add(thaw_animation);
                juggler.delayCall(thaw_complete,2.0);
                thaw_animation.addEventListener(Event.COMPLETE,function(){
                        thaw_animation.currentFrame = 4;
                        thaw_animation.play();
                        });
            }
            
            d2.onComplete = function(){
                removeChild(d2,true);
                juggler.remove(d2);
                addChild(d3);
                juggler.add(d3);
            }
            
            d3.onComplete = function(){
                removeChild(d3,true);
                juggler.remove(d3);
                addChild(d4);
                juggler.add(d4);
            }
            
            d4.onComplete = function(){
                removeChild(d4,true);
                juggler.remove(d4);
                callback();
            }
        }

    }
}

// Its a sprite with a built in juggler. Automaticly tracks its lifetime,
// Fill it with movie clips on its own juggler, and then add the entire
// sprite to a juggler.
class AnimSprite extends Sprite implements IAnimatable {

    public var juggler : Juggler;
    public var elapsedTime(get, null) : Float;
    public var animations : Map<String,Array<AnimationInfo>>;
    public var current_animation : String;
    public var current_animation_scale : Vec2;

    public function new(){
        super();
        juggler = new Juggler();
        animations = new Map<String,Array<AnimationInfo>>();
        current_animation = "";
        current_animation_scale = {x:1,y:1};
    }

    public function advanceTime(t) {
        juggler.advanceTime(t);
    }

    function get_elapsedTime() {
        return juggler.elapsedTime;
    }

    function addAnimation(animation_name : String, movie_clip : MovieClip){
        if(!animations.exists(animation_name)) 
            animations.set(animation_name, new Array<AnimationInfo>());
        var scale = {x: movie_clip.scaleX, y:movie_clip.scaleY};
        animations.get(animation_name).push({movieClip: movie_clip,scale: scale});
        movie_clip.visible = false;
        movie_clip.stop();
        juggler.add(movie_clip);
    }

    /* switiching to animationinfo so we can record its original scale
    function removeAnimation(animation_name : String, movie_clip){
        animations.get(animation_name).remove(movie_clip);
    }*/

    function playAnimation(animation_name : String, ?scale : Vec2) {
        if(scale == null) scale = {x:1,y:1};
        if(animation_name != current_animation ||
                !scale.equals(current_animation_scale)){
            if(current_animation != "") {
                for(animinfo in animations.get(current_animation)){
                    var anim = animinfo.movieClip;
                    anim.stop();
                    anim.visible = false;
                }
            }

            if(animation_name != "") {
                for(animinfo in animations.get(animation_name)){
                    var anim = animinfo.movieClip;
                    anim.play();
                    anim.visible = true;
                    anim.scaleX = animinfo.scale.x*scale.x;
                    anim.scaleY = animinfo.scale.y*scale.y;
                }
            }
        }
        current_animation = animation_name;
        current_animation_scale = scale;
    }

}

typedef AnimationInfo = {
    var movieClip : MovieClip;
    var scale : Vec2;
}
