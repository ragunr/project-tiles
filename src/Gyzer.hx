import starling.events.*;
import starling.animation.*;
import flash.ui.Keyboard;
import starling.textures.Texture;
import starling.textures.TextureAtlas;
import Math;
import Game;
import World;
import Geom;
using Geom.Vec2Ext;
using Geom.RectExt;
using Lambda;

class Gyzer extends Entity {

        public var active = false;

	public function new(){
        super();
        debugColor = 0xFF0000;
        setHitbox({x:-0, y:-2, w:1, h:2});
        density = 0;
        solid = false;
        restitution = 1;
        title = "gyzer";

        var frames = Root.assets.getTextures("gyzer.");

        var animation = Root.buildWorldMovieClipFromFrames(flash.Vector.ofArray([frames[0],frames[1]].array()));
        animation.pivotX = 0;
        animation.pivotY = 46;
        animation.setFrameDuration(0,0.4);
        animation.setFrameDuration(1,0.4);
        addChild(animation);
        addAnimation("ready", animation);

        var animation = Root.buildWorldMovieClipFromFrames(flash.Vector.ofArray([frames[2],frames[3]].array()));
        animation.pivotX = 0;
        animation.pivotY = 46;
        addChild(animation);
        addAnimation("fire", animation);

        playAnimation("ready");

        addEventListener(Event.ADDED, function(){
                removeEventListeners(Event.ADDED);
                var world = cast(parent, World);
                world.addPreCollisionListener(this, preCollision);
                });

        var switch_call = new DelayedCall(function() {
                active = !active;
                if(active) playAnimation("fire");
                else playAnimation("ready");
                }, 5.0);
        switch_call.repeatCount = 0;
        juggler.add(switch_call);
        
	}

    function preCollision(info : CollisionInfo, e1 : Entity, e2 : Entity){
        var other = e2;
        if(this == e2) other = e1;
        if(!active) return;
        other.impulse = other.impulse.add({x:0,y:-.15});

    }
    
}
