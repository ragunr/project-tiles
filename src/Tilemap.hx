import haxe.xml.Fast;
import flash.utils.ByteArray;
import flash.geom.Rectangle;
import starling.display.Image;
import starling.display.Sprite;
import starling.textures.Texture;
import starling.utils.AssetManager;

enum Orientation {
  Orthogonal;
  Isometric;
  IsometricStaggered;
  HexagonalStaggered;
}

enum RenderOrder {
  RightDown;
  RightUp;
  LeftDown;
  LeftUp;
}

private class Tile {
  public var id:Int;
  public var width:Int;
  public var height:Int;
  public var offset_x:Int = 0;
  public var offset_y:Int = 0;
  public var source:String;
  public var properties:Map<String,String>;
  // properties

  public function new() {
      properties = new Map<String,String>();
  }
}

private class Layer {
  public var name:String;
  public var data:Array<Array<Tile>>;
  public var visible:Bool;
  // properties

  public function new() {
    data = new Array<Array<Tile>>();
  }
}

private class Object{
  public var name:String;
  public var type:String;
  public var gid:Int;
  public var x:Int;
  public var y:Int;
  public var width:Int;
  public var height:Int;
  public var visible:Bool;
  public var customAttr:Map<String, String>;
  public function new(){
    customAttr = new Map<String, String>();
  }
}

private class Objectgroup{
  public var name:String;
  public var data:Array<Object>;
  public var visible:Bool;

  public function new(){
    data = new Array<Object>();
  }
}

class Tilemap extends Sprite {

  public var mapWidth(default, null):Int;
  public var mapHeight(default, null):Int;
  public var tileWidth(default, null):Int;
  public var tileHeight(default, null):Int;
  public var orientation(default, null):Orientation;
  public var renderOrder(default, null):RenderOrder;
  private var _tiles:Array<Tile>;
  private var _layers:Array<Layer>;
  private var _objectgroups:Array<Objectgroup>;
  private var _assets:AssetManager;
  private var _world:World;

  public function new(world:World, assets:AssetManager, xml:String) {
    super();
    _assets = assets;
    _world = world;

    var xml = Xml.parse(haxe.Resource.getString(xml));
    var source = new Fast(xml.firstElement());

    var txt:String;

    txt = source.att.orientation;
    if (txt == "") {
      orientation = Orientation.Orthogonal;
    } else if (txt == "orthogonal") {
      orientation = Orientation.Orthogonal;
    } else if (txt == "isometric") {
      orientation = Orientation.Isometric;
    } else if (txt == "isometric-staggered") {
      orientation = Orientation.IsometricStaggered;
    } else if (txt == "hexagonal-staggered") {
      orientation = Orientation.HexagonalStaggered;
    }

    txt = source.att.renderorder;
    if (txt == "") {
      renderOrder = RenderOrder.RightDown;
    } else if (txt == "right-down") {
      renderOrder = RenderOrder.RightDown;
    } else if (txt == "right-up") {
      renderOrder = RenderOrder.RightUp;
    } else if (txt == "left-down") {
      renderOrder = RenderOrder.LeftDown;
    } else if (txt == "left-up") {
      renderOrder = RenderOrder.LeftUp;
    }

    mapWidth = Std.parseInt(source.att.width);
    mapHeight = Std.parseInt(source.att.height);

    _world.map_width=mapWidth;
    _world.map_height=mapHeight;

    tileWidth = Std.parseInt(source.att.tilewidth);
    tileHeight = Std.parseInt(source.att.tileheight);

    _tiles = new Array<Tile>();
    for (tileset in source.nodes.tileset) {

      var _tileset_tiles = new Array<Tile>();
      if (tileset.has.source) {
        throw "External tileset source not supported.";
      }
      if (tileset.has.margin || tileset.has.spacing) {
        //throw "Only image collections are supported.";
          var twidth = Std.parseInt(tileset.att.tilewidth);
          var theight = Std.parseInt(tileset.att.tileheight);
          var margin = Std.parseInt(tileset.att.margin);
          var spacing = Std.parseInt(tileset.att.spacing);
          var img = tileset.node.image;

          var htiles = Std.int((Std.parseInt(img.att.width)-margin*2+spacing)/(twidth+spacing));
          var vtiles = Std.int((Std.parseInt(img.att.height)-margin*2+spacing)/(theight+spacing));

          for(j in 0...vtiles)
              for(i in 0...htiles)
              {
                  var t = new Tile();
                  t.id = j*htiles+i;
                  t.width = twidth;
                  t.height = theight;
                  t.offset_x = margin+(twidth+spacing)*i;
                  t.offset_y = margin+(theight+spacing)*j;
                  t.source = img.att.source;
                  t.source = t.source.substr(0,t.source.length-4);
                  _tileset_tiles.push(t);
                  _tiles.push(t);
              }

          for(tile in tileset.nodes.tile) {
              var _id = Std.parseInt(tile.att.id);
              for(prop in tile.node.properties.nodes.property){
                  _tileset_tiles[_id].properties.set(prop.att.name,prop.att.value);
              }
          }
          
      }
      else {
        for (tile in tileset.nodes.tile) {
          if (tile.has.id) {
            var t = new Tile();
            t.id = Std.parseInt(tile.att.id);
            for (image in tile.nodes.image) {
              t.width = Std.parseInt(image.att.width);
              t.height = Std.parseInt(image.att.height);
              t.source = image.att.source;
              t.source = t.source.substr(0, t.source.length-4);
            }
            //for(prop in tile.node.properties.nodes.property){
            //    t.properties.set(prop.att.name,prop.att.value);
            //}

            _tileset_tiles.push(t);
            _tiles.push(t);
          }
        }
      }
    }

    _layers = new Array<Layer>();
    for (layer in source.nodes.layer) {
      var t = new Layer();
      t.name = layer.att.name;
      t.visible = true;
      if(layer.has.visible) t.visible=false;
      for (i in 0...mapHeight) {
        t.data.push(new Array<Tile>());
        for (j in 0...mapWidth) {
          t.data[i].push(null);
        }
      }
      var i = 0;
      for (data in layer.nodes.data) {
        for (tile in data.nodes.tile) {
          t.data[Std.int(i / mapWidth)][Std.int(i % mapWidth)] = _tiles[Std.parseInt(tile.att.gid)-1];
          i += 1;
        }
      }
      _layers.push(t);
    }

    _objectgroups = new Array<Objectgroup>();
    for (objectgroup in source.nodes.objectgroup) {
      var t = new Objectgroup();
      t.name = objectgroup.att.name;
      t.visible=true;
      if(objectgroup.has.visible)visible=false;
      for(object in objectgroup.nodes.object){
        var o = new Object();
        o.gid = Std.parseInt(object.att.gid);
        o.x = Std.parseInt(object.att.x);
        o.y = Std.parseInt(object.att.y);
        o.name = (object.has.name)?object.att.name:"";
        o.type = (object.has.type)?object.att.type:"";
        o.width = (object.has.width)?Std.parseInt(object.att.width) : 1;
        o.height = (object.has.height)?Std.parseInt(object.att.height) : 1;
        o.visible = (object.has.visible)?false:true;
        
        if(object.hasNode.properties){
          for(property in object.node.properties.nodes.property){
            o.customAttr[property.att.name.toLowerCase()]=property.att.value;
          }
        }

        t.data.push(o);
      }
      _objectgroups.push(t);
    }



    for (layer in _layers) {
      if(!layer.visible) continue;
      // The default is renderOrder == RenderOrder.RightDown
      var xi = 0;
      var xf = mapWidth;
      var dx = 1;
      var yi = 0;
      var yf = mapHeight;
      var dy = 1;
      
      if (renderOrder == RenderOrder.RightUp) {
        xi = 0;
        xf = mapWidth;
        dx = 1;
        yi = mapHeight-1;
        yf = -1;
        dy = -1;
      }

      if (renderOrder == RenderOrder.LeftDown) {
        xi = mapWidth-1;
        xf = -1;
        dx = -1;
        yi = 0;
        yf = mapHeight;
        dy = 1;
      }

      if (renderOrder == RenderOrder.LeftUp) {
        xi = mapWidth-1;
        xf = -1;
        dx = -1;
        yi = mapHeight-1;
        yf = -1;
        dy = -1;
      }

      var _x = 0;
      var _y = 0;
      while (_y != yf) {
        while (_x != xf) {
          var cell = layer.data[_y][_x];
          if (cell != null && cell.source != null && cell.source != "") {
            /*Convert to Entities and adding to _world
            var img = new Image(_assets.getTexture(cell.source));
            img.pivotY = img.height;
            img.x = _x*tileWidth;
            img.y = _y*tileHeight + 32;
            addChild(img);*/
            var tile_entity = new World.Entity();
            if(cell.properties.get("visible") != "false") {
                var tex = Root.assets.getTexture(cell.source.substr(cell.source.lastIndexOf('/')+1));
                tex = Texture.fromTexture(tex,new Rectangle(cell.offset_x,cell.offset_y,cell.width,cell.height));
                tile_entity.layerMid.addChild( Root.buildWorldImageFromTexture(tex) );
            }
            tile_entity.x = _x;
            tile_entity.y = _y;
            var _hb_x = (cell.properties.exists("hitbox_x")) ? 
                Std.parseFloat(cell.properties.get("hitbox_x")) : 0;
            var _hb_y = (cell.properties.exists("hitbox_y")) ? 
                Std.parseFloat(cell.properties.get("hitbox_y")) : 0;
            var _hb_w = (cell.properties.exists("hitbox_w")) ? 
                Std.parseFloat(cell.properties.get("hitbox_w")) : 1;
            var _hb_h = (cell.properties.exists("hitbox_h")) ? 
                Std.parseFloat(cell.properties.get("hitbox_h")) : 1;
            //trace(_hb_w);
            if(cell.properties.get("interactive") != "false") tile_entity.setHitbox({x:_hb_x,y:_hb_y,w:_hb_w,h:_hb_h});
            if(cell.properties.get("type") == "damage") tile_entity.title = "damage";
            if(cell.properties.get("type") == "ledge") tile_entity.makeLedge(_world);

            _world.addChild(tile_entity);
            _world.juggler.add(tile_entity);
          }
          _x += dy;
        }
        _x = xi;
        _y += dy;
      }
    }
    for(objectgroup in _objectgroups){
      if(!objectgroup.visible) continue;
      for(object in objectgroup.data){
        if(object.name.toUpperCase()=="PLAYER"){
          _world.player = new Player();
          _world.player.x=object.x/24+1;
          _world.player.y=object.y/24-1;
          _world.addChild(_world.player);
          _world.juggler.add(_world.player);
          _world.camera_x = _world.player.x;
          _world.camera_y = _world.player.y;
        }
        else if(object.name.toUpperCase()=="ENEMY"){
          var enemy = new Enemy();
          enemy.x = object.x/24+1;
          enemy.y = object.y/24-1;
          _world.addChild(enemy);
          _world.juggler.add(enemy);
        }
        else if(object.name.toUpperCase()=="COIN"){
          var coin = new Coin();
          coin.x = object.x/24;
          coin.y = object.y/24-1;
          _world.addChild(coin);
          _world.juggler.add(coin);
        }
        else if(object.name.toUpperCase()=="HEALTHPACK"){
          var healthpack = new Healthpack();
          healthpack.x = object.x/24;
          healthpack.y = object.y/24-1;
          _world.addChild(healthpack);
          _world.juggler.add(healthpack);
        }
        else if(object.name.toUpperCase()=="GYZER"){
          var enemy = new Gyzer();
          enemy.x = object.x/24;
          enemy.y = object.y/24;
          _world.addChild(enemy);
          _world.juggler.add(enemy);
        }
        else if(object.name.toUpperCase()=="GOAL"){
          var entity = new World.Entity();
          entity.x = object.x/24+1;
          entity.y = object.y/24-1;
          entity.setHitbox({x:-5,y:-5,w:10,h:10});
          entity.solid = false;
          entity.title = "goal";
          var img = Root.buildWorldImage(object.type);
          img.alignPivot();
          entity.layerMid.addChild(img);
          _world.addChild(entity);
          _world.juggler.add(entity);
        }
        else{
          var obj = new World.Entity();
          obj.title = object.name.toLowerCase();
          var cell =_tiles[object.gid-1];
          obj.layerMid.addChild( Root.buildWorldImage( cell.source.substr(cell.source.lastIndexOf('/')+1) ));
          obj.x = object.x/24;
          obj.y = (object.y/24)-1;
          obj.setHitbox({x:0,y:0,w:object.width,h:object.height});

          for(key in object.customAttr.keys()){
            key = key.toLowerCase();
            if(key=="density") obj.density = Std.parseFloat(object.customAttr[key]);
            else if (key=="restitution") obj.restitution = Std.parseFloat(object.customAttr[key]);
            else if (key=="friction") obj.friction = Std.parseFloat(object.customAttr[key]);
            else if (key=="solid") obj.solid = (object.customAttr[key].toUpperCase()=="TRUE" || Std.parseInt(object.customAttr[key])==1)?true:false;
          }

          _world.addChild(obj);
          _world.juggler.add(obj);
        }
      }
    }

  }

}
