import starling.display.*;
import starling.events.Event;
import Geom;
import Game;
import Player;

using Geom.Vec2Ext;
using Geom.RectExt;

// Physics enabled world!
class World extends AnimSprite {


    public static inline var GRID_SIZE = 3;

    ////////////////////////////////////////////
    // Properties
    public var game : Game;
    public var gravity : Vec2;
    public var player : Player;
    public var enemy : Enemy;
    public var preCollisionListeners : Map<Entity, Array<CollisionListener>>;
    public var postCollisionListeners : Map<Entity, Array<CollisionListener>>;

    public var map_width:Float;
    public var map_height:Float;
    public var camera_x:Float;
    public var camera_y:Float;
    public var ox:Float;
    public var oy:Float;

    private var entities : Map<Int,Array<Entity>>;
    private var dynamic_entities : Array<Entity>;


    ////////////////////////////////////////////
    // Public functions

    public function new(game:Game,gravity:Vec2){
        super();
        this.game = game;
        this.gravity = gravity;
        entities = new Map<Int,Array<Entity>>();
        dynamic_entities = new Array<Entity>();
        preCollisionListeners = new Map<Entity, Array<CollisionListener>>();
        postCollisionListeners = new Map<Entity, Array<CollisionListener>>();

        ox = flash.Lib.current.stage.stageWidth/2;
        oy = flash.Lib.current.stage.stageHeight/2;
    }

    override public function advanceTime(timeElapsed:Float){
        //trace("tick");
        while (timeElapsed > 0) {
            var t = Math.min(timeElapsed, 0.005);
            timeElapsed -= t;

            //t /= 4;
            super.advanceTime(t);
            applyForces(t);
            resolveCollisions();

            resolveCamera();
        }
    }

    public function addPreCollisionListener(entity : Entity, callback : CollisionListener){
        if(!preCollisionListeners.exists(entity))
            preCollisionListeners.set(entity,new Array<CollisionListener>());
        preCollisionListeners.get(entity).push(callback);
    }

    public function addPostCollisionListener(entity : Entity, callback : CollisionListener){
        if(!postCollisionListeners.exists(entity))
            postCollisionListeners.set(entity,new Array<CollisionListener>());
        postCollisionListeners.get(entity).push(callback);
    }

    ////////////////////////////////////////////
    // Private functions

    private static function gridhash(x:Int,y:Int):Int{
        return y*0xFFFF+x;
    }

    private function addEntity(entity : Entity){
        if(entity.dynam || entity.getMass() > 0) {
            dynamic_entities.push(entity);
            entity.addEventListener(Event.REMOVED,function(event:Event){
                    dynamic_entities.remove(entity);
                    });
        }
        else {
            if(entity.getHitbox() == null) return;
            var grid_x = Std.int(entity.getHitbox().center().x / GRID_SIZE);
            var grid_y = Std.int(entity.getHitbox().center().y / GRID_SIZE);
            for(i in -2...2) for(j in -2...2) {
                var p = gridhash(grid_x+j,grid_y+i);
                if(!entities.exists(p)) entities[p] = new Array<Entity>();
                entities[p].push(entity);
            }
            entity.addEventListener(Event.REMOVED,function(event:Event){
                    for(i in -2...2) for(j in -2...2) {
                        entities[gridhash(grid_x+j,grid_y+i)].remove(entity);
                    }
                    });
        }
    }

    override public function addChild(entity:DisplayObject):DisplayObject{
        addEntity(cast(entity,Entity));
        return super.addChild(entity);
    }

    private function getEntities():List<Entity>{
        var result = new List<Entity>();
        for(i in 0...numChildren){
            var child:Entity;
            try { child = cast(getChildAt(i),Entity); }
            catch(ex:String) { throw 
                "World type objects can not contain children that are not "+
                "of type Entity: "+ex; }
            if(
                    (child.x-player.x)*(child.x-player.x) +
                    (child.y-player.y)*(child.y-player.y) >
                    14*14) continue;
            result.add(child);
        }
        return result;
    }

    private function dispatchPreCollisionEvent(info : CollisionInfo, e1 : Entity, e2 : Entity){
        if(preCollisionListeners.exists(e1)) for(listener in preCollisionListeners.get(e1)){
            listener(info,e1,e2);
        }
        if(preCollisionListeners.exists(e2)) for(listener in preCollisionListeners.get(e2)){
            listener(info,e1,e2);
        }
    }

    private function dispatchPostCollisionEvent(info : CollisionInfo, e1 : Entity, e2 : Entity){
        if(postCollisionListeners.exists(e1)) for(listener in postCollisionListeners.get(e1)){
            listener(info,e1,e2);
        }
        if(postCollisionListeners.exists(e2)) for(listener in postCollisionListeners.get(e2)){
            listener(info,e1,e2);
        }
    }

    private function applyForces(t:Float){
        for(child in dynamic_entities){
            if(
                    (child.x-player.x)*(child.x-player.x) +
                    (child.y-player.y)*(child.y-player.y) >
                    14*14) continue;
            var mass = child.getMass();

            // distance equasion x = x0 + v0*t+1/2*a*t^2
            var a : Vec2 = {x:0, y:0};
            if(mass > 0) {
                a = gravity;
                a = a.add(child.impulse.div(mass*t));
            }
            var v0 = child.velocity;
            var p0 = child.getVec2();

            var v = v0.add(a.mult(t));
            var p = p0.add(v0.mult(t)).add(a.mult(0.5*t*t));

            // update child
            child.impulse = {x:0, y:0}
            child.velocity = v;
            p.apply(child);
        }
    }

    private function resolveCollisions(){
        // Dont check the same entities twice
        var processed = new Map<Entity,Bool>();

        for(e1 in dynamic_entities){
            if(
                    (e1.x-player.x)*(e1.x-player.x) +
                    (e1.y-player.y)*(e1.y-player.y) >
                    14*14) continue;
            processed.set(e1,true);
            var grid_x = Std.int(e1.getHitbox().center().x / GRID_SIZE);
            var grid_y = Std.int(e1.getHitbox().center().y / GRID_SIZE);
            var grid_h = gridhash(grid_x,grid_y);
            /*if(e1.title == "player") {
                trace(e1.getHitbox().center().y);
                trace(grid_x+ ","+ grid_y);
            }*/
            //trace(grid_x+","+grid_y);
            //trace(grid_h);
            //throw(entities[gridhash(1,3)]);
            var grid_entities : Array<Entity> = (entities.exists(grid_h)) ? 
                entities[grid_h] : new Array<Entity>();
            for(e2 in grid_entities.concat(dynamic_entities)){
                // Don't run agaisnt entities already run as a primary entity
                if(processed.exists(e2)) continue;

                if(e1.getHitbox().intersects(e2.getHitbox())) 
                {
                    resolveCollision(e1, e2);
                }
            }
        }
    }

    private function resolveCollision(e1 : Entity, e2 : Entity) {
        var pen = e1.getHitbox().collisionNormal(e2.getHitbox());

        var info : CollisionInfo = {
            normal: pen.normalize(),
            vel : e1.velocity.sub(e2.velocity),
            restitution: Math.max(e1.restitution, e2.restitution),
            friction: Math.min(e1.friction, e2.friction),
            enabled: e1.solid && e2.solid,
            result_imp: 0.0,
            result_friction_imp: {x:0, y:0},
        }

        // Fire pre-collision events
        dispatchPreCollisionEvent(info, e1, e2);
        if(!info.enabled) return;

        // assume if e2 is static that it has infinite mass
        var invmass : Float;
        if(e2.getMass() == 0) {
            invmass = 1/e1.getMass();
        } else {
            invmass = 1/e1.getMass() + 1/e2.getMass();
        }

        if(info.vel.dot(info.normal) > 0){
            // Calculates impulse based on the two bodies, and applys that
            // impulse equal and opposite on each body
            // equasion based on http://chrishecker.com/images/e/e7/Gdmphys3.pdf
            info.result_imp = info.vel.mult(-(1+info.restitution)).dot(info.normal) / 
                info.normal.dot(info.normal.mult(invmass));

            // Calculate the friction magnitude based on normal force
            var f_max = Math.abs(info.result_imp*info.friction);
            var perp_m = info.vel.project_perp(info.normal).mag()/invmass;
            var f_imp = Math.min(f_max,perp_m);
            info.result_friction_imp = info.vel.project_perp(info.normal).normalize(f_imp);
        }

        // Fire post-collision events
        dispatchPostCollisionEvent(info, e1, e2);
        if(!info.enabled) return;

        // Move out of penetration along normal
        pen.mult(e2.getMass()/(e1.getMass()+e2.getMass())).apply_offset(e2);
        pen.mult(-e1.getMass()/(e1.getMass()+e2.getMass())).apply_offset(e1);

        // Apply the equal/opposite collision impulses
        e1.impulse = e1.impulse.add(info.normal.mult(info.result_imp));
        e2.impulse = e2.impulse.add(info.normal.mult(-info.result_imp));

        // Apply friction impulses
        e1.impulse = e1.impulse.add(info.result_friction_imp.mult(-1));
        e2.impulse = e2.impulse.add(info.result_friction_imp);
    }

    private function resolveCamera(){
        camera_x = player.x;
        camera_y = player.y;
        this.x = -Math.min(Math.max(camera_x*this.scaleX, ox), (map_width*this.scaleX)-ox)+ox;
        this.y = -Math.min(Math.max(camera_y*this.scaleY, oy), (map_height*this.scaleY)-oy)+oy+49;
    }


    ////////////////////////////////////////////
    // Static functions

    public static function build_test_scene(world:World){
        var floor = new Entity();
        floor.setHitbox({x:0,y:0,w:10,h:2});
        floor.x = 2;
        floor.y = 10;
        floor.layerMid.addChild(Root.buildWorldImage("pc_tile"));
        world.addChild(floor);
        world.juggler.add(floor);

        var box = new Entity();
        box.setHitbox({x:0,y:0,w:1,h:2});
        box.x = 4;
        box.y = 5;
        box.density = 2;
        box.friction = 1.0;
        world.addChild(box);
        world.juggler.add(box);
        world.addPostCollisionListener(box, function(info:CollisionInfo, e1 : Entity, e2 : Entity){
                //trace(info.result_friction_imp);
                });

        world.player = new Player();
        world.player.x = 7;
        world.player.y = 4;
        world.addChild(world.player);
        world.juggler.add(world.player);

        world.enemy = new Enemy();
        world.enemy.x = 14;
        world.enemy.y = 9;
        world.addChild(world.enemy);
        world.juggler.add(world.enemy);
    }
}

typedef CollisionInfo = {
    var normal : Vec2;
    var vel : Vec2;
    var restitution : Float;
    var friction : Float;
    var enabled : Bool;
    var result_imp : Float;
    var result_friction_imp : Vec2;
}

typedef CollisionListener = CollisionInfo->Entity->Entity->Void;


class Entity extends AnimSprite {

    ////////////////////////////////////////////
    // Properties

    // simulation information
    private var hitbox : Rect;
    public var velocity : Vec2 = {x:0,y:0};
    public var impulse : Vec2 = {x:0,y:0};
    public var density : Float = 0;
    public var restitution : Float = 0;
    public var friction : Float = Math.POSITIVE_INFINITY;
    public var solid : Bool=true;
    public var dynam : Bool=false;

    // display layers
    public var layerBack : Sprite3D;
    public var layerMid : Sprite3D;
    public var layerFront : Sprite3D;
    private var layerDebug : Sprite;

    public var debugDraw(default,set):Bool;
    public var debugColor : Int = 0x000000;

    public var title : String = "";

    ////////////////////////////////////////////
    // Public functions

    public function new (){
        super();

        layerBack = new Sprite3D();
        layerMid = new Sprite3D();
        layerFront = new Sprite3D();
        layerDebug = new Sprite();

        debugDraw = false;
    }

    function set_debugDraw(setting:Bool):Bool{
        removeChildren();
        if(setting){
            addChild(layerDebug);
            addChild(layerBack);
            addChild(layerMid);
            addChild(layerFront);
        } else {
            addChild(layerBack);
            addChild(layerMid);
            addChild(layerFront);
        }
        return setting;
    }

    public function setHitbox(rect:Rect){
        hitbox = rect;
        var debugQuad = new Quad(hitbox.w,hitbox.h,debugColor);
        hitbox.topLeft().apply(debugQuad);
        layerDebug.removeChildren(0,-1,true);
        layerDebug.addChild(debugQuad);
    }

    public function getHitbox():Rect{
        if(hitbox == null) return null;
        return hitbox.offset(getVec2());
    }

    public function getMass():Float{
        if(hitbox == null) return 0;
        return hitbox.area() * density;
    }

    public function makeLedge(world:World) {
        world.addPreCollisionListener(this,ledgeCollisionListener);
    }

    function ledgeCollisionListener(info : CollisionInfo, e1 : Entity, e2 : Entity){
        var other = (e1 == this) ? e2 : e1;
        var flipped = (e1 == this) ? 1 : -1;
        if(info.normal.y*flipped >= 0) info.enabled = false;
        //trace(info);
    }
    
}

