import flash.display.Sprite;
import flash.events.*;
import flash.net.*;
import World;

class Level{
	private static var tiles=["b"=>"pc_tile"];
	public function new(world:World, level_name:String){
		var level = new Loader(level_name, function(levelArray:Array<Array<String>>){
				for(i in 0...levelArray.length){
					for(j in 0...levelArray[i].length){
						//Add Tiles to World
						//Array entries should be something along the lines of "", "D", etc..
						//Then Tile Textures can be looked up in the Tiles Map
						//Coordinates should be based on tiles are 16x16
						if(levelArray[i][j]=="P"){
							//Initialize Player
							world.player = new Player();
							world.player.x=j;
							world.player.y=i;
							world.addChild(world.player);
							world.juggler.add(world.player);

						}
						else if(levelArray[i][j]!=""){
							var tile_entity = new Entity();
							tile_entity.x=j;
							tile_entity.y=i;
							tile_entity.setHitbox({x:0,y:0,w:1,h:1});
							tile_entity.layerMid.addChild( Root.buildWorldImage(tiles[levelArray[i][j]]) );
							world.addChild(tile_entity);
							world.juggler.add(tile_entity);
						}
					}
				}
			});
	}
}

class Loader{
	public function new(level_name:String, onComplete:Array<Array<String>>->Void){
		var csvLoader:URLLoader = new URLLoader();
		csvLoader.addEventListener(Event.COMPLETE, function(e:Event){
				var str:String = csvLoader.data;
				var arr:Array<String> = str.split("\n");
				var finalArr:Array<Array<String>> = new Array<Array<String>>();
				for(i in 0...arr.length){
					var t = arr[i];
					//Split is bugged in this version of Haxe(?), doesn't get rid of the Delimiter
					//So checking for a '\n' Character here and chopping it off it is there
					if (t.charCodeAt(t.length-1)==13) {t=t.substring(0,t.length-1);}
					finalArr[i] = t.split(",");
				}
				onComplete(finalArr);
				});
		csvLoader.load(new URLRequest("/assets/levels/"+level_name+".csv"));
	}
}