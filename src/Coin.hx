import starling.events.*;
import flash.ui.Keyboard;
import starling.display.*;
import starling.textures.Texture;
import starling.textures.TextureAtlas;
import Math;
import Game;
import World;
import Geom;
using Geom.Vec2Ext;
using Geom.RectExt;
import Player;

class Coin extends Entity {

	public static var BODY_FRICTION = 0.0;

	public function new(){
        super();
        solid = false;
        debugColor = 0x00000F;
        setHitbox({x:0, y:0, w:.5, h:.5});
        density = 0;
        restitution = 0;
        friction = BODY_FRICTION;
        title = "coin";

        var animation = Root.buildWorldMovieClip("disc.");
        addChild(animation);
        addAnimation("spin", animation);

        playAnimation("spin");
        addEventListener(Event.ADDED, function(){
                removeEventListeners(Event.ADDED);
                var world = cast(parent, World);
                world.addPreCollisionListener(this, preCollision);
                });
        
	}

    function preCollision(info : CollisionInfo, e1 : Entity, e2 : Entity){
        var other = e2;
        if(this == e2) other = e1;
        if(other.title == "player"){
            Root.assets.playSound("pickup_coin");
            removeChildren();
        }       
    }
    
}