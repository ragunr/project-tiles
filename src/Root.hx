import bitmasq.Gamepad;
import bitmasq.GamepadEvent;
import starling.display.*;
import starling.utils.AssetManager;
import starling.core.Starling;
import starling.animation.Transitions;
import starling.textures.TextureSmoothing;
import starling.textures.Texture;
import starling.text.TextField;
import starling.events.*;
import flash.ui.Keyboard;
import flash.ui.GameInput;
import flash.ui.GameInputControl;
import flash.ui.GameInputDevice;
import flash.events.GameInputEvent;
import flash.media.SoundChannel;
import Math;

import Game;

class Root extends Sprite {

    public static var assets:AssetManager;
    public static var gameInput: GameInput;
    public static var singleton : Root;
    public var currentMenu:String;
    public var currentButton:Int;
    public var menu:Menu;
    public var game:Game;
    public var tutorial:Tutorial;
    public var credits:Credits;
    public var gameOver:GameOver;
    public var continueScreen:ContinueScreen;
    public var music_channel:SoundChannel=null;
    public var music_title:String="";
    public var volume : Float = 0.5;
    public var levelBackgrounds: Array<String> = ["city_ruins_dulled_background", "city_ruins_dulled_background", "asteroid_background", "city_ruins_dulled_background"];
    public var levels: Array<String> = ["Tiled_Test", "City_Level", "Asteroid_Level","Final_Level"];
    public var currentLevel:Int = 1;

    public function new() {
        Gamepad.init(flash.Lib.current.stage);
        super();
      
    }
    public function initialize(startup:Startup) {
        singleton = this;
        Gamepad.get().addEventListener(GamepadEvent.CHANGE,gamepadChange);
        /*gameInput = new GameInput();
        gameInput.addEventListener(GameInputEvent.DEVICE_ADDED, 
                function(event:GameInputEvent){
                event.device.enabled=true;
                for(i in 0...event.device.numControls){
                    var control = event.device.getControlAt(i);
                    //control.id
                    var gamepad_map = new Map<String,Int>();
                    gamepad_map.set("BUTTON_4", Keyboard.SPACE);
                    gamepad_map.set("BUTTON_5", Keyboard.SLASH);
                    gamepad_map.set("BUTTON_16", Keyboard.UP);
                    gamepad_map.set("BUTTON_17", Keyboard.DOWN);
                    gamepad_map.set("BUTTON_18", Keyboard.LEFT);
                    gamepad_map.set("BUTTON_19", Keyboard.RIGHT);
                    if(gamepad_map.exists(control.id))
                        control.addEventListener(flash.events.Event.CHANGE,function(e:Dynamic){
                            var kevent = new KeyboardEvent(
                                (control.value == 0)? KeyboardEvent.KEY_UP : KeyboardEvent.KEY_DOWN,
                                0,
                                gamepad_map.get(control.id));
                            dispatchEvent(kevent);

                                });
                    }
                });
        gameInput.addEventListener(GameInputEvent.DEVICE_REMOVED, function(e:Dynamic){
                });
        */
        assets = new AssetManager();
        // enqueue here
        assets.enqueue("assets/assets.png");
        assets.enqueue("assets/assets.xml");
        assets.enqueue("assets/sounds/Whip2.mp3");
        assets.enqueue("assets/sounds/Jump1.mp3");
        assets.enqueue("assets/sounds/Pickup1.mp3");
        assets.enqueue("assets/sounds/Hurt1.mp3");
        assets.enqueue("assets/sounds/Death1.mp3");
        assets.enqueue("assets/sounds/Select1.mp3");
        assets.enqueue("assets/sounds/Enemy_Hurt.mp3");
        assets.enqueue("assets/sounds/Blast.mp3");
        assets.enqueue("assets/sounds/Tick1.mp3");
        assets.enqueue("assets/music/music_cave.mp3");
        assets.enqueue("assets/music/music_theme.mp3");
        assets.enqueue("assets/health_empty.png");
        assets.enqueue("assets/health_full.png");
        assets.enqueue("assets/font_lg_wht.png");
        assets.enqueue("assets/font_lg_wht.fnt");
        assets.enqueue("assets/font_lg.png");
        assets.enqueue("assets/font_lg.fnt");
        assets.enqueue("src_assets/enemy/enemy1_walk1.png");
        assets.enqueue("src_assets/enemy/enemy1_walk2.png");
        assets.enqueue("src_assets/enemy/enemy1_walk3.png");
        assets.enqueue("src_assets/enemy/enemy1_walk4.png");
        assets.enqueue("src_assets/enemy/enemy1_walk21.png");
        assets.enqueue("src_assets/enemy/enemy1_walk22.png");
        assets.enqueue("src_assets/enemy/enemy1_walk23.png");
        assets.enqueue("src_assets/enemy/enemy1_walk24.png");
        assets.enqueue("src_assets/enemy/blast10.png");
        assets.enqueue("src_assets/enemy/blast11.png");
        assets.enqueue("assets/disc.01.png");
        assets.enqueue("assets/disc.02.png");
        assets.enqueue("assets/disc.03.png");
        assets.enqueue("assets/disc.04.png");
        assets.enqueue("assets/disc.05.png");
        assets.enqueue("assets/disc.06.png");
        assets.enqueue("assets/disc.07.png");
        assets.enqueue("assets/disc.08.png");
        assets.enqueue("assets/disc.09.png");
        assets.enqueue("assets/disc.10.png");
        assets.enqueue("assets/disc.11.png");
        assets.enqueue("assets/disc.12.png");
        assets.enqueue("assets/gameover.png");
        assets.enqueue("assets/the_end.png");
        assets.enqueue("assets/menubutton.png");
        assets.enqueue("assets/menubutton_selected.png");
        assets.enqueue("assets/healthpack.01.png");
        assets.enqueue("assets/healthpack.02.png");
        assets.enqueue("assets/healthpack.03.png");
        assets.enqueue("assets/healthpack.04.png");
        assets.enqueue("assets/healthpack.05.png");
        assets.enqueue("assets/startbutton.png");
        assets.enqueue("assets/startbutton_selected.png");
        assets.enqueue("assets/continue.png");
        assets.enqueue("assets/continuebutton.png");
        assets.enqueue("assets/continuebutton_selected.png");
        assets.enqueue("assets/tutorialbutton.png");
        assets.enqueue("assets/tutorialbutton_selected.png");
        assets.enqueue("assets/credits.png");
        assets.enqueue("assets/backbutton.png");
        assets.enqueue("assets/backbutton_selected.png");
        assets.enqueue("assets/background.png");
        assets.enqueue("assets/creditsbutton.png");
        assets.enqueue("assets/creditsbutton_selected.png");
        assets.enqueue("assets/menubutton.png");
        assets.enqueue("assets/tutorialBackground.png");
        assets.enqueue("assets/pickup_coin.mp3");
        assets.enqueue("assets/asteroid_tileset.png");
        assets.enqueue("assets/asteroid_background.png");
        assets.loadQueue(function onProgress(ratio:Float) {
            if(ratio == 1) {
                Starling.juggler.tween(startup.loadingBitmap,
                    1.0,
                    {
                        transition: Transitions.EASE_OUT,
                        delay: 1.0,
                        alpha: 0,
                        onComplete: function()
                        {
                            startup.removeChild(startup.loadingBitmap);
                        }
                    });
                //start();
                flash.media.SoundMixer.soundTransform = new flash.media.SoundTransform(volume);
                addMenu();
                currentMenu = "main";
                currentButton = 0;
                addEventListener(Event.TRIGGERED, menuButtonClicked);
                addEventListener(KeyboardEvent.KEY_DOWN, keyboardInput);
                menu.startButton.state = "over";
            }
        });
    }

    public function start(){
        removeChildren(0,-1,true);
        removeEventListeners();
        Starling.juggler.purge();
        game = new Game();
        Starling.juggler.add(game);
        addChild(game);
    }

    private function gamepadChange(event:GamepadEvent){
        var gamepad_map = new Map<Int,Int>();
        gamepad_map.set(Std.int(Gamepad.A_DOWN), Keyboard.SPACE);
        gamepad_map.set(Std.int(Gamepad.A_RIGHT), Keyboard.SLASH);
        gamepad_map.set(Std.int(Gamepad.D_UP), Keyboard.UP);
        gamepad_map.set(Std.int(Gamepad.D_DOWN), Keyboard.DOWN);
        gamepad_map.set(Std.int(Gamepad.D_LEFT), Keyboard.LEFT);
        gamepad_map.set(Std.int(Gamepad.D_RIGHT), Keyboard.RIGHT);

        if(gamepad_map.exists(Std.int(event.control)))
        {

            var kevent = new KeyboardEvent(
                (event.value == 0)? KeyboardEvent.KEY_UP : KeyboardEvent.KEY_DOWN,
                0,
                gamepad_map.get(Std.int(event.control)));
            dispatchEvent(kevent);
        }
        
    }

    public function playMusic(title){
        if(title == music_title) return;
        if(music_channel != null) music_channel.stop();
        music_channel = assets.playSound(title,0,0x3FFFFFFF);
        music_channel.soundTransform = new flash.media.SoundTransform(2);
        music_title = title;
    }

    public static function buildWorldImage(tex:String){
        return buildWorldImageFromTexture(assets.getTexture(tex));
    }

    public static function buildWorldImageFromTexture(tex:Texture){
        var img = new Image(tex);
        img.smoothing = TextureSmoothing.NONE;
        img.scaleX = img.scaleY = 1.0/24.0;
        img.readjustSize();
        return img;
    }

    public static function buildWorldMovieClip(tex:String){
        return buildWorldMovieClipFromFrames(assets.getTextures(tex));
    }

    public static function buildWorldMovieClipFromFrames(frames:flash.Vector<Texture>){
        var img = new MovieClip(frames);
        img.smoothing = TextureSmoothing.NONE;
        img.scaleX = img.scaleY = 1.0/24.0;
        img.readjustSize();
        return img;
    }

    public static function buildWorldTextField(text:TextField){
        //img.smoothing = TextureSmoothing.NONE;
        //text.scaleX = text.scaleY = 1.0/24.0;
        text.redraw();
        return text;
    }
    public function addMenu() {

        menu = new Menu();
        menu.alpha = 0;
        addChild(menu);
        playMusic("music_theme");
        //Tween in menu
        Starling.juggler.tween(menu, 0.25, {
                    transition: Transitions.EASE_IN,
                        delay: 0.0,
                        alpha: 1.0
        });
    }

    public function keyboardInput(event:KeyboardEvent){
    	if (event.keyCode == Keyboard.DOWN){
    		
    		if(currentMenu=="main"){
                currentButton++;
    			currentButton = currentButton % 3;
    		}
    		if(currentMenu=="credits" || currentMenu == "tutorial"|| currentMenu == "gameover" || currentMenu == "continue"){
    			currentButton = 0;
    		}
    	}

    	if (event.keyCode == Keyboard.UP){
    		
    		if(currentMenu=="main"){
                currentButton--;
    			if (currentButton < 0){
    				currentButton = 2;
    			}
    			currentButton = currentButton % 3;
    		}

    		if(currentMenu=="credits" || currentMenu == "tutorial" || currentMenu == "gameover" || currentMenu == "continue"){
    			currentButton = 0;
    		}    		
    	}

    	if (currentMenu == "main"){
    		if(currentButton == 0){
    			menu.startButton.state = "over";
    			menu.creditsButton.state = "up";
    			menu.tutorialButton.state = "up";
    		}
    		else if(currentButton == 1){
    			menu.startButton.state = "up";
    			menu.creditsButton.state = "up";
    			menu.tutorialButton.state = "over";
    		}
    		else if(currentButton == 2){
    			menu.startButton.state = "up";
    			menu.creditsButton.state = "over";
    			menu.tutorialButton.state = "up";
    		}
    		else{
    			menu.startButton.state = "up";
    			menu.creditsButton.state = "up";
    			menu.tutorialButton.state = "up";
    		}
    	}
    	else if(currentMenu=="credits"){
    		credits.backButton.state = "over";
   		}
    	else if(currentMenu=="tutorial"){
    		tutorial.backButton.state = "over";
   		}

    	if (event.keyCode == Keyboard.SPACE){
    		if(currentMenu == "main"){
    			if (currentButton == 0){
    				startGame();
    			}
    			else if(currentButton == 1){
    				currentButton = 0;
    				currentMenu = "tutorial";
    				showTutorial();
    			}
    			else if(currentButton == 2){
    				currentButton = 0;
    				currentMenu = "credits";
    				showCredits();
    			}

    		}
    		else if (currentMenu == "credits" || currentMenu == "tutorial" || currentMenu == "gameover"){
    			currentButton = 0;
    			currentMenu = "main";
    			removeChildren(0,-1,true);
    			addMenu();
            }
            else if (currentMenu == "continue"){
                    removeChildren(0,-1,true);
                    currentLevel+= 1;
                    if(currentLevel < levels.length){
                        start();
                    }
                    else{
                        currentLevel = 1;
                        Root.singleton.showGameOver(true);
                    }
            }
    		
    	}
    }

    public function menuButtonClicked(event:Event) {
        var button = cast(event.target, Button);
        if(button.name == "start") {
            startGame();
        } 
        else if(button.name == "tutorial") {
            showTutorial();
         }
        else if(button.name == "credits") {
            showCredits();
        } 
        else if(button.name == "continue") {
            removeChildren(0,-1,true);
            removeEventListeners();
            currentLevel+= 1;
            if(currentLevel < levels.length){
                start();
            }
            else{
                currentLevel = 1;
                Root.singleton.showGameOver(true);
                }
        } 
        else if(button.name == "back") {
            Starling.juggler.tween(getChildAt(0), .25, {
                    transition: Transitions.EASE_OUT,
                        delay: 0.0,
                        alpha: 0.0,
                        onComplete: function() {
                            removeChildAt(0,true);
                            }
        
            });
            addMenu();
        }
        else if(button.name == "return"){
            removeChildren(0,-1,true);
            removeEventListeners();
            addMenu();
            currentMenu = "main";
            currentButton = 0;
            addEventListener(Event.TRIGGERED, menuButtonClicked);
            addEventListener(KeyboardEvent.KEY_DOWN, keyboardInput);
            menu.startButton.state = "over";
        }
    }

    public function startGame() {
        //Tween out menu
        removeChildren(0,-1,true);
        removeEventListeners();
        start();
    }

    public function showTutorial() {
        //Tween out the menu
        Starling.juggler.tween(getChildAt(0), 0.25, {
                    transition: Transitions.EASE_OUT,
                        delay: 0.0,
                        alpha: 0.0,
                        onComplete: function() {
                            removeChildAt(0,true);
                        }
        });
        tutorial = new Tutorial();
        tutorial.alpha = 0;
        addChild(tutorial);
        //Tween in tutorial screen
        Starling.juggler.tween(tutorial, 0.25, {
                    transition: Transitions.EASE_IN,
                        delay: .25,
                        alpha: 1.0
        });
    }

    public function showGameOver(win:Bool) {
        currentMenu = "gameover";
        currentButton = 0;
        gameOver = new GameOver(win);
        gameOver.alpha = 0;
        addChild(gameOver);
        addEventListener(Event.TRIGGERED, menuButtonClicked);
        addEventListener(KeyboardEvent.KEY_DOWN, keyboardInput);
        //Tween in gameover screen
        Starling.juggler.tween(gameOver, 0.25, {
                    transition: Transitions.EASE_IN,
                        delay: .25,
                        alpha: 1.0
        });
    }

    public function showContinue() {
        Root.singleton.game.world.removeChildren();
        currentMenu = "continue";
        currentButton = 0;
        addEventListener(Event.TRIGGERED, menuButtonClicked);
        addEventListener(KeyboardEvent.KEY_DOWN, keyboardInput);        
        continueScreen = new ContinueScreen();
        addChild(continueScreen);

    }

    public function showCredits() {
        //Tween out the menu
        Starling.juggler.tween(getChildAt(0), 0.25, {
                    transition: Transitions.EASE_OUT,
                        delay: 0.0,
                        alpha: 0.0,
                        onComplete: function() {
                            removeChildAt(0,true);
                        }
        });
        credits = new Credits();
        credits.alpha = 0;
        addChild(credits);
        //Tween in tutorial screen
        Starling.juggler.tween(credits, 0.25, {
                    transition: Transitions.EASE_IN,
                        delay: .25,
                        alpha: 1.0
        });
    }
}

class Menu extends Sprite {
    public var background:Image;
    public var startButton:Button;
    public var tutorialButton:Button;
    public var creditsButton:Button;
    public var totalButtons:Int;
    public var currentButton:Int;

    public function new() {
        super();
        currentButton = 0;
        background = new Image(Root.assets.getTexture("city_ruins_background"));
        background.scaleX = 2;
        background.scaleY = 2;
        addChild(background);
        var title = new Image(Root.assets.getTexture("title"));
        title.x = .5*(flash.Lib.current.stage.stageWidth - title.width);
        title.y = 0;
        addChild(title);
        startButton = new Button(Root.assets.getTexture("startbutton"), "", null, Root.assets.getTexture("startbutton_selected"));
        startButton.name = "start";

        startButton.x = .5*(flash.Lib.current.stage.stageWidth - startButton.width) ;
        startButton.y = 300;
        this.addChild(startButton);

        tutorialButton = new Button(Root.assets.getTexture("tutorialbutton"), "", null, Root.assets.getTexture("tutorialbutton_selected"));
        tutorialButton.x = .5*(flash.Lib.current.stage.stageWidth - startButton.width);
        tutorialButton.y = 400;

        tutorialButton.name = "tutorial";
        this.addChild(tutorialButton);
    

        creditsButton = new Button(Root.assets.getTexture("creditsbutton"), "", null, Root.assets.getTexture("creditsbutton_selected"));

        creditsButton.x = .5*(flash.Lib.current.stage.stageWidth - startButton.width);
        creditsButton.y = 500;

        creditsButton.name = "credits";
        this.addChild(creditsButton);

    }
}

class ContinueScreen extends Sprite {
    public var background:Image;
    public var foreground:Image;
    public var continueButton: Button;

    public function  new() {
        super();
        background = new Image(Root.assets.getTexture("city_ruins_background"));
        background.scaleX = 2;
        background.scaleY = 2;
        addChild(background);

        foreground = new Image(Root.assets.getTexture("continue"));
        addChild(foreground);

        continueButton = new Button(Root.assets.getTexture("continuebutton"), "", null, Root.assets.getTexture("continuebutton_selected"));
        continueButton.name = "continue";
        continueButton.x = .5*(flash.Lib.current.stage.stageWidth - continueButton.width);
        continueButton.y = 300;
        addChild(continueButton);
    }
}

class Tutorial extends Sprite {

    public var background:Image;
    public var backButton:Button;
    public var tutorialBackground:Image;

    public function new() {
        super();
        background = new Image(Root.assets.getTexture("city_ruins_background"));
        background.scaleX = 2;
        background.scaleY = 2;
        addChild(background);

        tutorialBackground = new Image(Root.assets.getTexture("tutorialBackground"));
        addChild(tutorialBackground);

        backButton = new Button(Root.assets.getTexture("backbutton"), "", null, Root.assets.getTexture("backbutton_selected"));
        backButton.name = "back";
        backButton.x = 0;
        backButton.y = 500;
        this.addChild(backButton);
    }
}

class Credits extends Sprite {

    public var background:Image;
    public var backButton:Button;
    public var creditsBackground:Image;

    public function new() {
        super();
        background = new Image(Root.assets.getTexture("city_ruins_background"));
        background.scaleX = 2;
        background.scaleY = 2;
        addChild(background);

        creditsBackground = new Image(Root.assets.getTexture("credits"));
        addChild(creditsBackground);

        backButton = new Button(Root.assets.getTexture("backbutton"), "", null, Root.assets.getTexture("backbutton_selected"));
        backButton.name = "back";
        backButton.x = 0;
        backButton.y = 500;
        this.addChild(backButton);
    }
}

class GameOver extends Sprite {
    public var background:Image;
    public var returnButton:Button;

    public function new(win:Bool) {
        super();

        
        var background:Image;
        var returnButton:Button;
        if(win) {
            background = new Image(Root.assets.getTexture("the_end"));
            returnButton = new Button(Root.assets.getTexture("menubutton"), "", null, Root.assets.getTexture("menubutton_selected"));
            returnButton.x = .5*(flash.Lib.current.stage.stageWidth - returnButton.width);
            returnButton.y = 300;
            returnButton.name = "return";
        } 
        else {
            background = new Image(Root.assets.getTexture("gameover"));
            returnButton = new Button(Root.assets.getTexture("menubutton"), "", null, Root.assets.getTexture("menubutton_selected"));
            returnButton.x = .5*(flash.Lib.current.stage.stageWidth - returnButton.width);
            returnButton.y = 300;
            returnButton.name = "return";
            returnButton.state = "over";
        }
        addChild(background);
        this.addChild(returnButton);
    }
}
