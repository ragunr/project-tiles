import starling.display.*;
import starling.text.TextField;
import starling.core.Starling;
import starling.utils.*;
import starling.events.*;
import flash.ui.Keyboard;
import Root;

class Score extends Sprite{
	public var score:Int;
	public var scoreText:TextField;
	public function new(){
		super();
		score = 0;
		scoreText = new TextField(256, 32, "Score: " + score, "font_lg", 16, 0xFFFFFF);
		scoreText.hAlign = HAlign.LEFT;
		addChild(scoreText);
		//addEventListener(KeyboardEvent.KEY_DOWN, keyDown); //debugging
	}

	public function update(){
		scoreText.text = "Score: " + score;		
	}

	public function addPoints(points:Int){
		score += points;
		update();
	}

	/*function keyDown(event : KeyboardEvent){ //debugging
		trace("Keydown");
		addPoints(100);
	}*/
}