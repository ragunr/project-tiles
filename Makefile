FLEX_SDK ?= C:/flexsdk

APP=game
APP_XML=$(APP).xml
ADL=$(FLEX_SDK)/bin/adl.exe
AMXMLC=$(FLEX_SDK)/bin/amxmlc
SOURCES=src/*.hx

all: $(APP).swf

$(APP).swf: $(SOURCES)
	haxe \
		-cp src \
		-cp vendor \
		-swf-version 11.8 \
		-swf-header 1024:576:60:ffffff \
		-main Startup \
		-swf $(APP).swf \
		-swf-lib vendor/starling.swc --macro "patchTypes('vendor/starling.patch')" \
		-swf-lib vendor/gamepad.swc \
		-D fdb\
		-resource assets/levels/Tiled_Test.tmx@Tiled_Test \
		-resource assets/levels/city_level.tmx@City_Level \
		-resource assets/levels/final_level.tmx@Final_Level \
		-resource assets/levels/asteroid_level.tmx@Asteroid_Level

clean:
	rm -rf $(APP).swf

test: $(APP).swf
	$(ADL) -profile tv -screensize 1024x576:1024x576 $(APP_XML)

